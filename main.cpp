#include <QImage>
#include <QVector>
#include <QColor>

#include <iostream>

int main(int argc, char *argv[])
{
    if(argc != 3)
    {
        std::cout << "ERROR: invalid program arguments" << std::endl;
        return -1;
    }

    QVector<QColor> colors;
    QImage input;
    QImage input2;
    if( !input.load(argv[1]) )
    {
        std::cout << "ERROR: input file 1 not found" << std::endl;
        return -2;
    }

    if( !input2.load(argv[2]) )
    {

        input2 = input;
    }

    int h = input2.height();
    if(input2.height() < input.height()) h = input.height();

    auto fin = input2.scaled(input.width()+input2.width(), h);
    fin.fill(0);

    for(int x = 0; x < input.width(); ++x)
    {
        for(int y = 0; y < input.height(); ++y)
        {
            auto c = input.pixel(x,y);
            fin.setPixel(x,y, c);
        }
    }

    for(int x = 0; x < input2.width(); ++x)
    {
        for(int y = 0; y < input2.height(); ++y)
        {
            auto c = input2.pixel(x,y);
            fin.setPixel(x+input.width(),y,c);
        }
    }

    fin.save(argv[2]);
    return  0;

}
